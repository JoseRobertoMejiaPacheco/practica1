package mx.edu.delasalle.loginapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import mx.edu.delasalle.loginapp.R

class LoginActivity : AppCompatActivity() {

    private lateinit var bnSignIn: Button
    private lateinit var edName: EditText
    private lateinit var edPassword : EditText

    private val TAG = "LoginApp"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        bnSignIn = findViewById(R.id.login_activity_bn_sign_in)
        edName = findViewById(R.id.login_activity_ed_name)
        edPassword = findViewById(R.id.login_activity_ed_password)

        //bnSignIn.setOnClickListener {}
        bnSignIn.setOnClickListener(::onClick)

        Log.e(TAG,"This is an error")
        Log.d(TAG,"This is for debuging")
        Log.w(TAG,"This is a warning")
        Log.i(TAG,"This is informative")
    }

    private fun onClick(view: View){
        val text = (view as Button).text
        val userName = edName.text.toString()
        val password = edPassword.text.toString()

        var intent = Intent(this, MainActivity::class.java)

        intent.putExtra("userName",userName)
        intent.putExtra("password",password)

        startActivity(intent)

        // Toast.makeText(this,"$text",Toast.LENGTH_LONG).show()
    }
}